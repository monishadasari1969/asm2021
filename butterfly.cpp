#include <bits/stdc++.h>
//#include <iostream>

using namespace std;

void node(int n,int k)
{
	if(n==0) 
	{
		for(int ii=0;ii<pow(2,(k-1));ii++)
		{	
			int t = 2*ii+1;
			printf("L%d: X0_%d\n L%d: X0_%d\n",t-1,ii,t,ii);
			//cout<<"hello";
		}
		for(int ii=0;ii<pow(2,(k-1));ii++)
		{
			//cout<<"hello";
			//int temp  = (int)(log2(ii)),t = 2*ii+1,p=2^(k-1);//temp is msb
			int temp = (ii & (1<<(k-2)))>>(k-2),p=pow(2,(k-2)),t=2*ii+1;
			if(temp==0) printf("X0_%d: L%d,L%d,X1_%d,X1_%d\n",ii,t-1,t,ii,ii+p);
			else printf("X0_%d: L%d,L%d,X1_%d,X1_%d\n",ii,t-1,t,ii,ii-p);
		}	 
	}
	else 
	{
		for(int ii=0;ii<pow(2,(k-1));ii++)
		{	
			int t = 2*ii+1;
			printf("R%d: X%d_%d\n R%d: X%d_%d\n",t-1,k-1,ii,t,k-1,ii);
		}
		for(int ii=0;ii<pow(2,(k-1));ii++)
		{
			int t = 2*ii+1;
			if(ii%2==0) printf("X%d_%d: R%d,R%d,X%d_%d,X%d_%d\n",k-1,ii,t-1,t,k-2,ii,k-2,ii+1);
			else printf("X%d_%d: R%d,R%d,X%d_%d,X%d_%d\n",k-1,ii,t-1,t,k-2,ii,k-2,ii-1);
		}	 
		
	}
	return;
}
int main()
{
	int k;//the dim of butterfly is 2^k*2^k
	//the switches X1_bin where bin is binary number
	//the nodes will be N1_bin and N2_bin
	//for k, switches X1_bin bin woulb be k bits
	cin>>k;
	//vector <vector <int>> graph;
	int t=pow(2,(k-1));
	//cout<<"hi";
	for(int ii=0;ii<k;ii++)
	{
	 	if(ii==0||ii==(k-1)) 
	 	{
	 		node(ii,k);
	 		cout<<"hi";
	 		continue;
	 	}
	 	cout<<t;
 		for(int jj=0;jj<t;jj++)
 		{
 			cout<<"hello";
 			int temp ,jj_next;
 			//cout<<jj<<"mo\n";= jj>>(k-2-ii)
 			temp = ((jj & (1 << (k -2 -ii ))) >> (k - 2 - ii));
 			if(temp==0) jj_next = jj + pow(2,(k-ii-2));
 			else jj_next = jj - pow(2,(k-ii-2));
 			printf("X%d_%d: X%d_%d,X%d_%d\n",ii,jj,ii+1,jj,ii+1,jj_next);	
 		}
	}
	return 0;
}
