
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

// FILE *out_file = fopen("output.txt", "w");
FILE *out_file;

int folded_torus_network(int n, int m)
{
    printf("%d, %d", n, m);
    int folded_torus[n][m];
    int count = 0, head_node_n = 0, head_node_m = 0;

    for (int i = 1; i <= n; i++)    
    {    
        for (int j = 1; j <= m; j++)    
        {    
            count += 1;
            folded_torus[i][j] = count;
            // folded_torus[i][j] = rand()%100 + 1;
        }    
    }
    printf("\n");
    for (int i = 1; i <= n; i++)    
    {    
        for (int j = 1; j <= m; j++)    
        {    
            printf("%d\t", folded_torus[i][j]);  
        }
        printf("\n");
    }
    
    if (n == m)
    {
        if (n%2 == 0)
        {
            head_node_n = n/2;
            head_node_m = m/2;
        }
        else
        {
            head_node_n = (n+1)/2;
            head_node_m = (m+1)/2;
        }
    }
    else
    {
        if (n%2 == 0)
        {
            head_node_n = n/2;
        }
        else
        {
            head_node_n = (n+1)/2;
        }

        if (m%2 == 0)
        {
            head_node_m = m/2;
        }
        else
        {
            head_node_m = (m+1)/2;
        }
    }
    printf("The Head Node: %d\n", folded_torus[head_node_n][head_node_m]);
 
    // printf("For First row\n");
    for (int i = 1; i <= m; i++)
    {
        if (i > 1 & i < m)
        {
            printf("The node %d connected to the %d, %d\n", folded_torus[1][i], folded_torus[1+1][i], folded_torus[n][i]);
            fprintf(out_file, "The node %d connected to the %d, %d\n", folded_torus[1][i], folded_torus[1+1][i], folded_torus[n][i]);
            // fclose(out_file);
        }
        else if (i == 1)
        {
            printf("The node %d connected to the %d, %d", folded_torus[1][i], folded_torus[1+1][i], folded_torus[n][i]);
            fprintf(out_file, "The node %d connected to the %d, %d", folded_torus[1][i], folded_torus[1+1][i], folded_torus[n][i]);
            for (int j = 3; j <= m; j=j+2)
            {
                printf(", %d", folded_torus[i][j]);
                fprintf(out_file, ", %d", folded_torus[i][j]);
            }  
            fprintf(out_file, "\n");
        }
        else
        {
            printf("The node %d connected to the %d, %d", folded_torus[1][i], folded_torus[1+1][i], folded_torus[n][i]);
            fprintf(out_file, "The node %d connected to the %d, %d", folded_torus[1][i], folded_torus[1+1][i], folded_torus[n][i]);
            for (int j = m-2; j >= 1; j=j-2)
            {
                printf(", %d", folded_torus[1][j]);
                fprintf(out_file, ", %d", folded_torus[1][j]);
            }
            printf("\n");
            fprintf(out_file, "\n");
        }
    }

    // printf("All middle row\n");
    for (int i = 2; i <= n-1; i++)
    {
        for (int j = 1; j <= m; j++)
        {
            if (j > 1 & j < m)
            {
                printf("The node %d connected to the %d, %d\n", folded_torus[i][j], folded_torus[i-1][j], folded_torus[i+1][j]);
                fprintf(out_file, "The node %d connected to the %d, %d\n", folded_torus[i][j], folded_torus[i-1][j], folded_torus[i+1][j]);
            }
            else if (j == 1)
            {
                printf("The node %d connected to the %d, %d", folded_torus[i][j], folded_torus[i-1][j], folded_torus[i+1][j]);
                fprintf(out_file, "The node %d connected to the %d, %d", folded_torus[i][j], folded_torus[i-1][j], folded_torus[i+1][j]);
                for (int k = 3; k <= m; k=k+2)
                {
                    printf(", %d", folded_torus[i][k]);
                    fprintf(out_file, ", %d", folded_torus[i][k]);
                }  
                printf("\n");
                fprintf(out_file, "\n");
            }
            else
            {
                printf("The node %d connected to the %d, %d", folded_torus[i][j], folded_torus[i-1][j], folded_torus[i+1][j]);
                fprintf(out_file, "The node %d connected to the %d, %d", folded_torus[i][j], folded_torus[i-1][j], folded_torus[i+1][j]);
                for (int k = m-2; k >= 1; k=k-2)
                {
                    printf(", %d", folded_torus[i][k]);
                    fprintf(out_file, ", %d", folded_torus[i][k]);
                }
                printf("\n");
                fprintf(out_file, "\n");
            }           
        }     
    }

    // printf("The Last Row\n");
    for (int i = 1; i <= m; i++)
    {
        if (i > 1 & i < m)
        {
            printf("The node %d connected to the %d, %d\n", folded_torus[n][i], folded_torus[n-1][i], folded_torus[1][i]);
            fprintf(out_file, "The node %d connected to the %d, %d\n", folded_torus[n][i], folded_torus[n-1][i], folded_torus[1][i]);
        }
        else if (i == 1)
        {
            printf("The node %d connected to the %d, %d", folded_torus[n][i], folded_torus[n-1][i], folded_torus[1][i]);
            fprintf(out_file, "The node %d connected to the %d, %d", folded_torus[n][i], folded_torus[n-1][i], folded_torus[1][i]);
            for (int j = 3; j <= m; j=j+2)
            {
                printf(", %d", folded_torus[n][j]);
                fprintf(out_file, ", %d", folded_torus[n][j]);
            }  
            printf("\n");
            fprintf(out_file, "\n");
        }
        else
        {
            printf("The node %d connected to the %d, %d", folded_torus[n][i], folded_torus[n-1][i], folded_torus[1][i]);
            fprintf(out_file, "The node %d connected to the %d, %d", folded_torus[n][i], folded_torus[n-1][i], folded_torus[1][i]);
            for (int j = m-2; j >= 1; j=j-2)
            {
                printf(", %d", folded_torus[n][j]);
                fprintf(out_file, ", %d", folded_torus[n][j]);
            }
            printf("\n");
            fprintf(out_file, "\n");
        }
    } 

}

int mesh_network(int n, int m)
{
    printf("%d, %d", n, m);
    int mesh[n][m];
    int count = 0, head_node_n = 0, head_node_m = 0;

    for (int i = 1; i <= n; i++)    
    {    
        for (int j = 1; j <= m; j++)    
        {     
            count += 1;
            mesh[i][j] = count;
        }    
    }
    printf("\n");
    for (int i = 1; i <= n; i++)    
    {    
        for (int j = 1; j <= m; j++)    
        {    
            printf("%d\t", mesh[i][j]);  
        }
        printf("\n");
    }

    if (n == m)
    {
        if (n%2 == 0)
        {
            head_node_n = n/2;
            head_node_m = m/2;
        }
        else
        {
            head_node_n = (n+1)/2;
            head_node_m = (m+1)/2;
        }
    }
    else
    {
        if (n%2 == 0)
        {
            head_node_n = n/2;
        }
        else
        {
            head_node_n = (n+1)/2;
        }

        if (m%2 == 0)
        {
            head_node_m = m/2;
        }
        else
        {
            head_node_m = (m+1)/2;
        }
    }
    printf("The Head Node: %d\n", mesh[head_node_n][head_node_m]);

    // Try to find the combination of the node connected with the other node
    // printf("For First row\n");
    for (int i = 1; i <= m; i++)
    {
        if (i > 1 & i < m)
        {
            printf("The node %d connected to the %d, %d, %d\n", mesh[1][i], mesh[1][i-1], mesh[1][i+1], mesh[1+1][i]);
            fprintf(out_file, "The node %d connected to the %d, %d, %d\n", mesh[1][i], mesh[1][i-1], mesh[1][i+1], mesh[1+1][i]);
        }
        else if (i == 1)
        {
            printf("The node %d connected to the %d, %d\n", mesh[1][i], mesh[1][i+1], mesh[1+1][i]);
            fprintf(out_file, "The node %d connected to the %d, %d\n", mesh[1][i], mesh[1][i+1], mesh[1+1][i]);
        }
        else
        {
            printf("The node %d connected to the %d, %d\n", mesh[1][m], mesh[1][m-1], mesh[1+1][m]);
            fprintf(out_file, "The node %d connected to the %d, %d\n", mesh[1][m], mesh[1][m-1], mesh[1+1][m]);
        }
    }

    // printf("All middle row\n");
    for (int i = 2; i <= n-1; i++)
    {
        for (int j = 1; j <= m; j++)
        {
            if (j > 1 & j < m)
            {
                printf("The node %d connected to the %d, %d, %d, %d\n", mesh[i][j], mesh[i][j-1], mesh[i][j+1], mesh[i-1][j], mesh[i+1][j]);
                fprintf(out_file, "The node %d connected to the %d, %d, %d, %d\n", mesh[i][j], mesh[i][j-1], mesh[i][j+1], mesh[i-1][j], mesh[i+1][j]);
            }
            else if (j == 1)
            {
                printf("The node %d connected to the %d, %d, %d\n", mesh[i][j], mesh[i-1][j], mesh[i+1][j], mesh[i][j+1]);
                fprintf(out_file, "The node %d connected to the %d, %d, %d\n", mesh[i][j], mesh[i-1][j], mesh[i+1][j], mesh[i][j+1]);
            }
            else
            {
                printf("The node %d connected to the %d, %d, %d\n", mesh[i][j], mesh[i-1][j], mesh[i+1][j], mesh[i][j-1]);
                fprintf(out_file, "The node %d connected to the %d, %d, %d\n", mesh[i][j], mesh[i-1][j], mesh[i+1][j], mesh[i][j-1]);
            }           
        }     
    }
    
    // printf("The Last Row\n");
    for (int i = 1; i <= m; i++)
    {
        if (i > 1 & i < m)
        {
            printf("The node %d connected to the %d, %d, %d\n", mesh[n][i], mesh[n][i-1], mesh[n][i+1], mesh[n-1][i]);
            fprintf(out_file, "The node %d connected to the %d, %d, %d\n", mesh[n][i], mesh[n][i-1], mesh[n][i+1], mesh[n-1][i]);
        }
        else if (i == 1)
        {
            printf("The node %d connected to the %d, %d\n", mesh[n][i], mesh[n][i+1], mesh[n-1][i]);
            fprintf(out_file, "The node %d connected to the %d, %d\n", mesh[n][i], mesh[n][i+1], mesh[n-1][i]);
        }
        else
        {
            printf("The node %d connected to the %d, %d\n", mesh[n][m], mesh[m][n-1], mesh[n-1][m]);
            fprintf(out_file, "The node %d connected to the %d, %d\n", mesh[n][m], mesh[m][n-1], mesh[n-1][m]);
        }
    }    
}


int ring_network(int n)
{
    int ring[n];
    int head_node = 0;
    for (int i = 1; i <= n; i++)
    {
        ring[i] = i;
    }

    for (int i = 1; i <= n; i++)
    {
        printf("%d\t", ring[i]);
    }

    srand(time(0));
    head_node = (rand() % (n + 1)) + 0;

    printf("\nHead Node: %d", ring[head_node]);
    
    // Try to find the combination of the node connected with the other node
    int i = 1;
    printf("\nThe Node: %d Connected to: %d, %d", ring[i], ring[i+1], ring[n]);
    fprintf(out_file, "\nThe Node: %d Connected to: %d, %d", ring[i], ring[i+1], ring[n]);
    for (int i = 2; i < n; i++)
    {
        printf("\nThe Node: %d Connected to: %d, %d", ring[i], ring[i-1], ring[i+1]);
        fprintf(out_file, "\nThe Node: %d Connected to: %d, %d", ring[i], ring[i-1], ring[i+1]);
    }
    i = n;
    printf("\nThe Node: %d Connected to: %d, %d\n", ring[i], ring[i-1], ring[0]);
    fprintf(out_file, "\nThe Node: %d Connected to: %d, %d\n", ring[i], ring[i-1], ring[0]);
}

int chain_network(int n)
{
    int chain[n];
    int head_node = 0;
    for (int i = 1; i <= n; i++)
    {
        chain[i] = i;
    }

    for (int i = 1; i <= n; i++)
    {
        printf("%d\t", chain[i]);
    }

    if (n%2 == 0)
    {
        head_node = n/2;
    }
    else
    {
        head_node = (n+1)/2;
    }
    printf("\nHead Node: %d", chain[head_node]);

    // Try to find the combination of the node connected with the other node
    int i = 1;
    printf("\nThe Node: %d Connected to: %d", chain[i], chain[i+1]);
    fprintf(out_file, "\nThe Node: %d Connected to: %d", chain[i], chain[i+1]);
    for (int i = 2; i < n; i++)
    {
        printf("\nThe Node: %d Connected to: %d, %d", chain[i], chain[i-1], chain[i+1]);
        fprintf(out_file, "\nThe Node: %d Connected to: %d, %d", chain[i], chain[i-1], chain[i+1]);
    }
    i = n;
    printf("\nThe Node: %d Connected to: %d\n", chain[i], chain[i-1]);
    fprintf(out_file, "\nThe Node: %d Connected to: %d\n", chain[i], chain[i-1]);
}


void main()
{

    out_file = fopen("output.txt", "w+");

    char ch;
    int n = 0 , m = 0;
    printf("\nPlz enter the L1 Network");
    printf("\nC for Chain\nR for Ring\nM for Mesh\nF for Folded Torus\nH for Hypercube\nB for Butterfly\n");
    scanf("%c", &ch);

    switch (ch)
    {
    case 'C':
        printf("\nChain");
        printf("\nPlz Enter the length of the chain network: ");
        scanf("%d", &n);
        chain_network(n);
    break;

    case 'R':
        printf("\nRing");
        printf("\nPlz Enter the length of the ring network: ");
        scanf("%d", &n);
        ring_network(n);
    break;        
    
    case 'M':
        printf("\nMesh");
        printf("\nPlz Enter the Configuration of the mesh network: ");
        scanf("%d %d", &n, &m);
        mesh_network(n, m);
    break; 

    case 'F':
        printf("\nFolded Torus");
        printf("\nPlz Enter the Configuration of the Folded Torus network: ");
        scanf("%d %d", &n, &m);
        folded_torus_network(n, m);
    break;     

    case 'H':
        printf("\nHypercube");
        printf("The size defined to the Hypercube is 8\n");
        // hypercube_network(8);
    break; 

    fclose(out_file);
    default:
        break;
    }
}